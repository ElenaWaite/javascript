Before writing the games functionality, a basic structure is needed to render the game inside
Use HTML and the canvas element 

ctx variable stores rendered 2d content
        #solid rectangle
          ctx.beginPath();
          ctx.rect(20, 40, 50, 50);
          ctx.fillStyle = "#FF0000";
          ctx.fill();
          ctx.closePath();
        #solid circle
          ctx.beginPath();
          ctx.arc(240, 160, 20, 0, Math.PI*2, false);
          ctx.fillStyle = "green";
          ctx.fill();
          ctx.closePath();
        #outline rectangle  
          ctx.beginPath();
          ctx.rect(160, 10, 100, 40);
          ctx.strokeStyle = "rgba(0, 0, 255, 0.5)";
          ctx.stroke();
          ctx.closePath();


circle JavaScript:
The most important part of the code above is the arc() method. It takes six parameters:

x and y coordinates of the arc's center
arc radius
start angle and end angle (what angle to start and finish drawing the circle, in radians)
direction of drawing (false for clockwise, the default, or true for anti-clockwise.) This last parameter is optional.

upt to step 5 
alt b to run

            var gameoverNotify = document.querySelector(' .game-over-notify');
            var interval;
            gameOverNotify.addEventListener('click', function() {
                document.location.reload();
            });