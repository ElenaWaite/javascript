# Introduction to JS

Client - e.g. chrome, firefox, safari 
Server - Node JS

How engines work?
Engines are quite complicated 
The engine reads the script
Then it converts the script to the machine language

Variables 
let message - mutable
const message - immutable 
1. The name must contain only letters, digits, sumbols, $ and _
2. The first character must not be a digit 
Don't shrink variables, make the semantic
Descriptive

Number 
There is no int or float
Has special values
Infinity
-Infinity
NaN

Undefined 
If you declare a variable it is always going to have type undefined by default
Can then change that variable to any type
Bad practice though 

Functions
function (parameters, comma, seperated){
    by default return undefined;
}

Array(Stack, Queue)
unshift and shift 
much slower than 
push and pop 



# JavaScript

- Much like JavaScript
JavaScript is written using the Unicode character set
JavaScript is a case-sensitive language 
It ignores spaces and line breaks

// This is an inline comment and will be ignored 

/*
* Also all comment
*/ 


## Data TypesThere are 6 primitive data types, these are data types which are not objects and do not have methods, these are immutable - number, string, boolean, symbol, null, undefined
The seventh data type is object

## Typeof operator
Determine the type associated with a variables current value

## Initialising Variables
You should declare a variable using the the var keyword before using it
If you do not initialise the variable it automatically takes on the variable undefined

## Coercion
In JavaScript, you can perform operations on values of different types without raising an exception.
The JavaScript interpreter implicitly converts or coerces one of the data types to that of the other then performs the operation
- If you add a number and a string the number is coerced to a string
- If you add a boolean and a string, the boolean is coerced to a string
- If you add a number and a boolean, the boolean is coerced to a number

##parseInt and parseFloat 
Convert a integer or a floating point number represented as a string to a number

## Operator Types
### Unary
A unary operator requires a single operand, either before or after the operator, following this format:
Operand operator 
Operator operand 
a++, ++ is a unary operator

### Binary
Requires two operand, one before the operator and one after the operator, following this format:
Operand1 operator operand2
A + b = c, + is a binary operator


### Ternary 
One ternary operator, the conditional operator
Expression - a ? B : c
The use of ? And : together constitutes the ternary operator

### Less Know Operators
% - use this operator for operand1 ** operand2
•• - exponentiation
++ - increment, use it in the postfix and prefix forms, increments the operand by one and returns the value of the operand
- -    decrement

## Loops 
For 
While 
Do-while
For-in
For-of 

Do - while 

Do { statment (s);
} while (condition);

For (var variable in object) {
	// inner code that uses variable here
}

For (let variables of iterable) {
	statement(s);
}

JavaScript is a programming language developed by Netscape inc. – it is not part of the Java platform.
Key differences between Java and JavaScript:
	Java is an OOP programming language while Java Script is an OOP scripting language.
	Java creates applications that run in a virtual machine or browser while JavaScript code is run on a browser only.
	Java code needs to be compiled while JavaScript code are all in text.
	They require different plug-ins.
JavaScript initially had a bad rep because of early quirks but is now a powerful, flexible and fast programming language
JavaScript powers the dynamic behaviour on most websites
Console
The console is the panel that displays important messages e.g. errors for developers. 
Most of the work a computer does is invisible to us by default, if we want to see things appear on our screen, we can print, or log, to our console directly.
In JavaScript, the console keyword refers to an object, a collection of data and actions, that we can use in our code. Keywords are words that are built into the JavaScript language, so the computer will recognize them and treats them specially.
One action, or method, that is built into the console object is the .log() method. When we write console.log() what we put inside the parentheses will get printed, or logged, to the console.
Printing values to the console is useful to see what work you are doing.
Like in CSS, adding a semicolon at the end of your code denotes the end of the line, and whilst in JavaScript your code will run without it. We recommend learning the habit of ending each statement with a semi-colon so you never leave one out when it is actually required.
Comments
Programming is often highly collaborative
In addition, our own code can become difficult to understand when we return to it 
Useful to leave notes in the code – these are ignored by JavaScript, they are just for humans
 There are two types of comment in JavaScript:
1.	A single line comment will comment on a single line and is denoted with two forward slashes // preceding it
2.	A multi-line comment will comment out multiple lines an d is denoted with /* to begin the comment and */ to end the comment.
Data Types
Data types are the classifications we give to the different kinds of data that we use in programming. In JavaScript, there are seven fundamental data types:
1.	Number: Any number, including numbers with decimals: 4, 8, 1516, 23.42.
2.	String: Any grouping of characters on your keyboard (letters, numbers, spaces, symbols, etc.) surrounded by single quotes: ' ... ' or double quotes " ... ". Though we prefer single quotes. Some people like to think of string as a fancy word for text.
3.	Boolean: This data type only has two possible values— either true or false (without quotes). It’s helpful to think of booleans as on and off switches or as the answers to a "yes" or "no" question.
4.	Null: This data type represents the intentional absence of a value, and is represented by the keyword null (without quotes).
5.	Undefined: This data type is denoted by the keyword undefined (without quotes). It also represents the absence of a value though it has a different use than null.
6.	Symbol: A newer feature to the language, symbols are unique identifiers, useful in more complex coding. No need to worry about these for now.
7.	Object: Collections of related data.

The first 6 of those types are considered primitive data types. They are the most basic data types in the language. Objects are more complex, and you’ll learn much more about them as you progress through JavaScript. At first, seven types may not seem like that many, but soon you’ll observe the world opens with possibilities once you start leveraging each one. As you learn more about objects, you’ll be able to create complex collections of data.
Arithmetic Operators
Basic arithmetic comes hand in hand with programming.
An operator is a character that performs a task in our code. JavaScript has several built-in arithmetic operators, that allow us to perform mathematical calculations on numbers.
Include the following operators and symbols:
1.	Add: +
2.	Subtract: -
3.	Multiply: *
4.	Divide: /
5.	Remainder: %

When we console.log() the computer will evaluate the expression inside the parentheses and print the result to the console.
If we want to print the characters e.g. 3 + 4 they would need to be wrapped in quotes and printed as a string 
The remainder operator, sometimes called modulo, returns the number that remains after the right-hand number divides into the left-hand number as many times as it evenly can: 11 % 3 equals 2 because 3 fits into 11 three times, leaving 2 as the remainder.
String Concatenation
Operators aren't just for numbers! When a + operator is used on two strings, it appends the right string to the left string:
This process of appending one string to another is called concatenation. Notice in the third example we had to make sure to include a space at the end of the first string. The computer will join the strings exactly, so we needed to make sure to include the space we wanted between the two strings.
Properties
Introduce a new piece of data into a JavaScript program, the browser saves it as an instance of the data type.
Every string instance has a property called length that stores the number of characters in that string. You can retrieve property information by appending the string with a full stop and the property lane (.length).
The . is another operator – the dot operator.
Methods
Methods
Strings also have methods, actions that we can perform.
We call, or use, these methods by appending an instance with a period (the dot operator), the name of the method, and opening and closing parentheses: ie. 'example string'.methodName()'.
Does that syntax look a little familiar? When we use console.log() we're calling the .log() method on the console object. 

JavaScript documentation has a list of built- in string methods that developers use as a reference tool.
Two examples are:
.toUpperCase() – Which will print a string as upper case
And
.startWith() – which will print true or false depending on whether the character in the parentheses matches the letter at the start of the string

Built-in Objects
In addition to console, there are other objects built into JavaScript. Down the line, you’ll build your own objects, but for now these “built-in" objects are full of useful functionality.
For example, if you wanted to perform more complex mathematical operations than arithmetic, JavaScript has the built-in Math object.
Objects have methods which can be called and used.
An example is the Math.random() method which will generate a random number between 0 and 10 unless a it is multiplied by a number in which case will find a random number between 0 and that number.
Math.floor() rounds to the nearest whole number

Review
Let's take one more glance at the concepts we just learned:

	Data is printed, or logged, to the console, a panel that displays messages, with console.log().
	You can write single-line comments with // and multi-line comments between /* and */.
	There are 7 fundamental data types in JavaScript: strings, numbers, booleans, null, undefined, symbol, and object.
	Numbers are any number without quotes: 23.8879
	Strings are characters wrapped in single or double quotes: 'Sample String'
	The built-in arithmetic operators include +, -, *, /, and %.
	Objects, including instances of data types, can have properties, stored information. The properties are denoted with a . after the name of the object.
	Objects, including instances of data types, can have methods, actions. Methods are called by adding a period after the name of the object and adding ().
	We can access properties and methods by using the ., dot operator.
	Built-in objects, including Math, are collections of methods and properties that JavaScript provides.

Variables
A variable is a container for a value.
You can think of variables as little containers for information that live in a computer’s memory.
Information stored in variables. Such as username, account number, or even personalised greeting can then be found in memory.
Variables also provide a way of labelling data with a  descriptive name, so our programs can be understood more clearly by the reader and ourselves.
There are only a few things you can do with variables:
Create a variable with a descriptive name.
Store or update information stored in a variable.
Reference or “get” information stored in a variable.

Variables are not values; they contain values and represent them with a name.
Observe the diagram with the colored boxes. Each box represents variables; the values are represented by the content, and the name is represented with the label.
In this lesson, we will cover how to use the var, let, and const keywords to create variables.
Create a variable: var
There were a lot of changes introduced in the ES6 version of JavaScript in 2015. One of the biggest changes was two new keywords, let and const, to create, or declare, variables. Prior to the ES6, programmers could only use the var keyword to declare variables.
There are a few general rules for naming variables:
	Variable names cannot start with numbers.
	Variable names are case sensitive, so myName and myname would be different variables. It is bad practice to create two variables that have the same name using different cases.
	Variable names cannot be the same as keywords. For a comprehensive list of keywords check out MDN's keyword documentation.
Note: In the next exercises, we will learn why ES6's let and const are the preferred variable keywords by many programmers. Because there is still a ton of code written prior to ES6, it's helpful to be familiar with the pre-ES6 var keyword.
Create a variable: let 
The let keyword was introduced in ES6. The let keyword signifies that the variable can be assigned a different value.
In that case, a variable can be assigned a value at a later date without using a key word.
Another concept that we should be aware of when using let (and even var) is that we can declare a variable without assigning the variable a value. In such a case, the variable will be automatically initialized with a value of undefined and we can reassign the value of the variable.
Create a Variable: const
Short for the word constant – a const variable cannot be reassigned, you will get a TypeError if you try.
You also cannot have a const variable without a  value
Mathematical Assignment Operators
We can use variable and maths operators to calculate new values and assign them to a variable.
To reassign variables the following operators can be used: +=, -=, *=, /=
The increment (++) and decrement (--) operator increase or decrease the value of the variable by one.
Can completely reassign a variable introduced with ‘let’ – by simlywriting the variable and using the operator = 
String Concatenation with Variables
Previous exercises – assigned string to variables
Can also connect (or concatenate) strings in variables
The + operator can be used to combine two string values even if those values are being stored in variables
String Interpolation
In the ES6 version of JavaScript – we can insert or interpolate, variables into strings using temperate literals.
A temperate literal is wrapped by backticks (`)
Inside the temperate literal, you’ll see a placeholder, ${myPet}. The value of myPet is inserted into the template literal.
One of the biggest benefits to using template literals is the readability of the code. Using template literals, you can more easily tell what the new string will be. You also don't have to worry about escaping double quotes or single quotes.
typeof operator
While writing code, it can be useful to keep track of the data types of the variables in your program
If you need to check the data type of a variable’s value, you can use the typeof operator
The typeof operator checks the value to its right and returns, or passes back a string of the data type

Review:
Let's review what we learned:
•	Variables hold reusable data in a program and associate it with a name.
•	Variables are stored in memory.
•	The var keyword is used in pre-ES6 versions of JS.
•	let is the preferred way to declare a variable when it can be reassigned, and const is the preferred way to declare a variable with a constant value.
•	Variables that have not been initialized store the primitive data type undefined.
•	Mathematical assignment operators make it easy to calculate a new value and assign it to the same variable.
•	The + operator is used to concatenate strings including string values held in variables
•	In ES6, template literals use backticks ` and ${} to interpolate values into a string.
•	The typeof keyword returns the data type (as a string) of a value.


Conditional Statements 
Decisions based on circumstances – if one thing is true, do this. If not, then do something else/
These if-else decisions can be modelled in code by creating conditional statements. A conditional statement checks specific condition(s) and performs a task based on the condition(s).
Exploring how programs make decisions by evaluating conditions and introducing logic into our code:
-	If, else if, and else statements
-	Comparison operators
-	Logical operators
-	Truthy vs falsy values
-	Ternary operators
-	The switch statement

### The if keyword
If statement syntax
if(true) {
	console.log('This message will print');
}

### if else - adds some default behaviour if a particular statement isn't true
if (false) {
  console.log('The code in this block will not run.');
} else {
  console.log('But the code in this block will!');
}

### Comparison Operators
When writing conditional statements, sometimes we need to use different types of operators to compare values. These operators are called comparison operators.

Here is a list of some handy comparison operators and their syntax:

Less than: <
Greater than: >
Less than or equal to: <=
Greater than or equal to: >=
Is equal to: ===
Is NOT equal to: !==

### Logical Operators
Working with conditionals means that we will be using booleans, true or false values. In JavaScript, there are operators that work with boolean values known as logical operators. We can use logical operators to add more sophisticated logic to our conditionals. There are three logical operators:

the and operator (&&) - checks if two things are true 
the or operator (||) - checks if either of two things are true
the not operator, otherwise known as the bang operator (!) - reverses the value of boolean, either tke a true value and pass back false or a false value and pass back true

### Truthy and Falsy 
When checking if a variable exists, even if the variable has been created, some values may evaluate to false:
0
Empty strings like ""
null
undefined
NaN

### Short-circuit evaluation 
let defaultName;
if (username) {
  defaultName = username;
} else {
  defaultName = 'Stranger';
}

let defaultName = username || 'Stranger';

Both mean the same, the || checks the left statement first, the variable defaultName will be assigned the value of username if its truthy, selse it is assigned stranger
This concept is short-circuit evaluation

### Ternary operator
Simplifies an if else statement

let isNightTime = true;

if (isNightTime) {
  console.log('Turn on the lights!');
} else {
  console.log('Turn off the lights!');
}

isNightTime ? console.log('Turn on the lights!') : console.log('Turn off the lights!');

The condition is provided before the question mark, two expressions are provided after seperated by a colon
If the first evaluates to true then it executes, if it doesnt then the second executes

### else if, more possible outcome to the if else
let stopLight = 'yellow';

if (stopLight === 'red') {
  console.log('Stop!');
} else if (stopLight === 'yellow') {
  console.log('Slow down.');
} else if (stopLight === 'green') {
  console.log('Go!');
} else {
  console.log('Caution, unknown!');
}

### The switch keyword
let groceryItem = 'papaya';

switch (groceryItem) {
  case 'tomato':
    console.log('Tomatoes are $0.49');
    break;
  case 'lime':
    console.log('Limes are $1.49');
    break;
  case 'papaya':
    console.log('Papayas are $1.29');
    break;
  default:
    console.log('Invalid item');
    break;
}

case keyword checks if the expression mactehs the value that comes after it, if it does then the case runs
The break keyword tells the computer to execute the bolck if the cas ehas been run, otherwise the computer will keep trying to match all the cases 

### Summary:

- An if statement checks a condition and will execute a task if that condition evaluates to true.
- if...else statements make binary decisions and execute different code blocks based on a provided condition.
- We can add more conditions using else if statements.
- Comparison operators, including <, >, <=, >=, ===, and !== can compare two values.
- The logical and operator, &&, or "and", checks if both provided expressions are truthy.
- The logical operator ||, or "or", checks if either provided expression is truthy.
- The bang operator, !, switches the truthiness and falsiness of a value.
- The ternary operator is shorthand to simplify concise if...else statements.
- A switch statement can be used to simplify the process of writing multiple else if statements. The break keyword stops the remaining cases from being checked and executed in a switch statement.

## Functions
Hoisting feature in javascript - allows functions to be accessed before they are defined
Not good practice

A function declaration consists of:
- The function keyword.
- The name of the function, or its identifier, followed by parentheses.
- A function body, or the block of statements required to perform a specific task, enclosed in the function’s curly brackets, { }.

function call =
the function name followed by parentheses

### Parameters
Set parameters in the parentheses of the function
can set default paremeters if a value is not included in the function call

### Return 
Return statement - captures whatever value we want the computer to return 
Whatever is written after it will not be executed
Allows functions to produce an output that can be saved to a variable for later use

## Helper Functions 
Functions called within another function 
We can use functions to section off small bits of logic or tasks

## Function Expressions
Usually anonymous functions

## Arrow Functions
=> removes the need to type out function every time you create a function 

Syntax:
const rectangleArea = (width, height) => {
  let area = width * height;
  return area
}

### Concise Body Arrow Functions
Functions that take only a single parameter do not need that parameter to be enclosed in parentheses. However, if a function takes zero or multiple parameters, parentheses are required.

A function body composed of a single-line block does not need curly braces. Without the curly braces, whatever that line evaluates will be automatically returned. The contents of the block should immediately follow the arrow => and the return keyword can be removed. This is referred to as implicit return.

### First class functions
const callback = () => 5;
const example = function(fn) {
	fn();
}

You can pass a function as a parameter 
You can also return a function

const BlogController = {
	index:(posts) => Views.index(posts),
	create: 
}

Something about not needing brackets or  methods to call a function - look up 

### Pure functions
Should favour pure functions in your script

#### Side Effects if you don't:
- Mutations
- Accessing system state
- 
-
-

Even if you have to have an impure function you should seperate it from the pure

### Pure Functions - Array Methods

### Closure = function and external variables

## Scope
Defines what variable scan be accessed or referenced.

### Blocks
{} structural marker in code, group one or more statements together, function

### Global Scope 
Variables declared outside of blocks
These variables can also be accessed inside of the blocks

### Block Scope 
Variable defined inside a block, only accessible within that block
Known as a local variable

### Scope Pollution
Don't want too many global variables
Makes it difficult to keep track of them 
Can fill up global namespace

### Practice Good Scoping
Tightly scoping your variables will greatly improve your code in several ways:

It will make your code more legible since the blocks will organize your code into discrete sections.
It makes your code more understandable since it clarifies which variables are associated with different parts of the program rather than having to keep track of them line after line!
It's easier to maintain your code, since your code will be modular.
It will save memory in your code because it will cease to exist after the block finishes running.

## Arrays
A list where the data can be of different types or of the same type
Can save an array to a variable
Each element has a numbered position known as its index

Can access an index an array with syntax = array[index]
Can also access inidvidual characters in a string using this syntax

## Update elements 
array[index] = new variable
For both const and let variables

## reassiging an array variable 
Only with arrays assigned to a let variable, const can't be reassigned

## .length
console.log(array.length)
Returns number of items in the array

## .push
allows you to add elements to the end of an array
array.push(nextitem, nextitem2)
Mutates original array
destructive array method

## .pop()
removes the lasy item of an array
array.pop();

## .shift()
array.shift()
remove the first item from the array list

## .unshift()
add soemthing to th ebgeinning of the array 
array.unshift(firstitem)

## .slice()
array.slice(index)
Only prints that index
Print part of a list = (1, 3)
Will print items at indexes 1 and 2 - does not print the end element
non-mutating - does not change the list

## .indexOf()
array.indexOf(somevalue)
returns the index of that value

## Arrays and Functions
When you pass an array to a function, if the array if mutated inside the function, that change will be maintained outside the function as well.

## Nested Array 
Acessing elements in an array in an array
array[1][0] = the first element in the second array


