(function() {
    const initialState ={
        board: [['-','-','-'],
                ['-','-','-'],
                ['-','-','-']],
        turn: 'X'
    };

    function print(state){
        state.board.forEach((row) => console.log(row));
    }

    function checkRowsForWin(state) {
        return state.board.some((row) => {
            return row.every((cell) => {
                return cell == 
            })
        })
    }
    function checkAllFilled(state) {
        return state.board.every((row) => {
            return row.every((cell) => {
                return cell !== '-';
            });
        });
    }

    function checkCollumnsForWin(state)

    function checkDiagonalsforWin(state)

    // pure, just return true or false, doesn't change state, just checks
    function checkIsGameOver(state) {
        if (checkRowsForWin(state) || checkCollumnsForWin(state) || checkDiagonalsforWin(state));
        console.log("Game over: Player " + state.turn + " won");
        return true;
    } elseif (checkAllFilled(state)) {
        console.log("Game over: DRAW");
        return true;
    } else {
        return false;
    }
}

// pure function, makes new board cos it doesn't mutate state
function changeTurn(state) {
    return {
        board: makeNewBoard(state.board),
        turn: state.turn === 'X' ? '0' : 'X'
    }
}

function takeInput(gameCallback) {
    process.stdin.setEncoding('utf8');
    process.stdin.once('data', (input) => {
        gameCallback(parseInput(input));
    });
}

function game(state) {
    const gameCallback = (input) => {
        const movedState = makeMove(state, input);
        const isGameOver = checkIsGameOver(movedState);
        if (!isGameOver) {
            game(changeTurn(movedState));
        }
    }

    print(state);
    takeInput(gameCallback);
}
    console.log(checkJsGameOver(initialState))

})()